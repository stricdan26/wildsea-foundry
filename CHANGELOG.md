# The Wildsea (Unofficial) - FoundryVTT

## v0.1.1

- Add Quick Reference rules Journal
- Attribute tooltips (Mutex)
- Fix access to Depth setting (#37)

## v0.1.0

- FoundryVTT v12 support

## v0.0.14

- Add setting to change UI Track position

## v0.0.13

- Apply ratingMods for non-design ship items

## v0.0.12

- Fix errors in compendium pack entries

## v0.0.11

- Add compendium packs to the CI

## v0.0.10

- Add compendium packs of character and ship aspects

## v0.0.9

- Leaf bullet icon
- Sortable slim items
- Improve UI Track dialog
- Add basic FoundryVTT v12 support
- Use Prosemirror editor for rich text fields
- Minimum Stake cost for ship items is now 0

## v0.0.8

- Allow UI Tracks to be "Open", "Hidden" or "Secret"
- Add drag-drop reordering for UI Tracks

## v0.0.7

- Add UI Tracks

## v0.0.6

- Fix dice roll outcomes

## v0.0.5

- Add ship rating rolls
- Update Ship rating modifiers
- Fix burn in chat messages
- Add outcome tooltips to chat messages
- Add option to disable Burn tooltip message

## v0.0.4

- Resolves shift click issue with burn in Firefox

## v0.0.3

- Add Burn for tracks
- Add missing translation
- Ship rating styling

## v0.0.2

- Adds dice rolling

## v0.0.1

- First usable version of the player character sheet
